/*file name: kuts_lab_7_2 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 11.12.2021
*дата останньої зміни 11.12.2021
*Лабораторна №7
*завдання :Розробити блок-схему алгоритму та реалізувати його мовою 
С\С++ для обробки елементів матриці рядків/стовпчиків/діагоналі відповідно
індивідуального завдання, що знаходиться у таблиці І.2 з додатку І. Розрахунки 
представити у вигляді одновимірного масиву, який необхідно вивести на очищений 
екран.
*призначення програмного файлу: Створити та вивести одномірний масив з сум додатних
елементів кожної паралельної діагоналі, які вище головної діагоналі матриці v35(n,m).
*варіант : №4
*/
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
using namespace std;
int main(){
system("cls");
SetConsoleCP(1251);
SetConsoleOutputCP(1251);
int n,m,j,i, pi,pj;
cout<<" введіть к-сть рядків =   \t";
cin>>n;
cout<<" введіть к-сть стопчиків =\t";
cin>>m;
cout<<endl;
int a[n][m],r[m],k=0, sum=0;
srand(time(NULL));
for(i=0;i<n;i++){
  for(j=0;j<m;j++){
    a[i][j] = rand()%100-50;
    printf("%4d",a[i][j]);  
  }
  cout<<endl;
}
cout<<endl;

int s[m];
for(i=0; i<m ; i++) s[i]=0;

for (k = 0; k < m; k++)  
{
   pi=0; pj=k;
   for (; (pi<n)&(pj<m);pi++,pj++)
   if (a[pi][pj]>0) 
   { 
     s[k]+=a[pi][pj];
   }
  
   
}
 printf(" массив : \n");
for (i=0; i<m; i++)
{
  printf("%4d",s[i]);
}
cout<<endl;
system("pause");
return 0;
}
