/*file name: kuts_lab_7_3 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 11.12.2021
*дата останньої зміни 11.12.2021
*Лабораторна №7
*завдання :Розробити блок-схему алгоритму та реалізувати його мовою С\С++ для обробки елементів
у визначеній частині матриці відповідно індивідуального завдання, що знаходиться у таблиці І.4 з додатку І.
*призначення програмного файлу: В заданій багатовимірній матриці g4(3,4,4) знайти середнє арифметичне значення
усіх додатних елементів. Вивести матрицю та середнє арифметичне.
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i,j , n,m;
	float cc;
	srand(time(NULL));
	cout<<" задайте к-сть рядків = ";
	cin>>n;
	cout<<" задайте к-сть стовпчиків = ";
	cin>>m;
	int a9[n][m] ,b[m] ,k[m];
	float c[m];
	printf(" матриця ( %d, %d): \n",n,m);
	for (i = 0; i < n; i++){
		for (j = 0; j < m; j++){
			b[j]=0;
			k[j]=0;
			
			a9[i][j] = rand()%100-50;
			printf("%5d", a9[i][j]);
			}
		cout<<endl;
		}
	for (i = 0; i < n; i++){
		for (j = i; j < m; j++){
			if ( ( (i+j)/2 ) < ( ( m+n ) /4 ) ){
				b[j]+= a9[i][j];
				k[j]++;
			}
		}
 	}
	cout<<" вихідна матриця :"<<endl;
	for (j = 0; j < m; j++){
		c[j]=(double)b[j]/(double)k[j];
		printf("%2.1f ",c[j]);
	}
	cout<<endl;
	system("pause");

	return 0;
}

