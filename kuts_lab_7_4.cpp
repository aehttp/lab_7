/*file name: kuts_lab_7_4 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 11.12.2021
*дата останньої зміни 11.12.2021
*Лабораторна №7
*завдання :Розробити блок-схему алгоритму та реалізувати його мовою С\С++ модифікації для
обробки тривимірного масиву відповідно індивідуального завдання, що знаходиться у таблиці І.4 з додатку І.
*призначення програмного файлу: В заданій багатовимірній матриці g4(3,4,4) знайти середнє арифметичне значення
усіх додатних елементів. Вивести матрицю та середнє арифметичне.
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
	// процедура переведення курсору в задану позицію на екрані консолі
void gotoxy(int xpos, int ypos)
{ COORD scrn;
	HANDLE hOuput = GetStdHandle(STD_OUTPUT_HANDLE);
	scrn.X = xpos; scrn.Y = ypos;
	SetConsoleCursorPosition(hOuput,scrn);
}
int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	const int N=3, M=4, H=4, D=4;
	int i, j, k ,n=0, sum = 0 , g9[N][M][H];
	double ser;
	srand(time(NULL));
	printf(" вхідна g4 ( %d, %d, %d): \n",N,M,H);
	for (i = 0; i < N; i++)
	for (j = 0; j < M; j++)
	for (k = 0; k < H; k++)
		{
		g9[i][j][k] = rand()%100-50;
		gotoxy(((j+k)*D+1),(((1+i)*M)-j));
		printf("%d", g9[i][j][k]);
			if(g9[i][j][k]> 0 ){
				sum+=g9[i][j][k];
				n++;
			}
		}
	ser=(double)sum/(double)n;
	gotoxy(0, N*M);
	printf(" середнє арифметичне додатніх елементів = %f \n",ser);
	system("pause");

	return 0;
}
