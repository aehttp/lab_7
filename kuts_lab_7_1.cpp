/*file name: kuts_lab_7_1 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 10.12.2021
*дата останньої зміни 10.12.2021
*Лабораторна №7
*завдання :Розробити блок-схему алгоритму та реалізувати його
мовою С\С++ для обробки усіх елементів матриці відповідно індивідуального
завдання, що знаходиться у таблиці І.1 з додатку І.
*призначення програмного файлу: Обчислити та вивести мінімальний елемент масиву .
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i,j , n,m ,min;
	srand(time(NULL));
	cout<<"задайте кількість рядків матриці n =    \t";
	cin>>n;
	cout<<"задайте кількість стовпчиків матриці m =\t";
	cin>>m;
	int a33[n][m];
	printf("матриця a33( %d, %d): \n",n,m);
	for (i = 0; i < n; i++){
		for (j = 0; j < m; j++){
			a33[i][j] = rand()%100-50;
			printf("%4d", a33[i][j]);
			}
		cout<<endl;
		}
	min=a33[0][0];
	for (i = 0; i < n; i++){
		for (j = 0; j < m; j++){
			if(a33[i][j]<min){
				min=a33[i][j];
			}
		}
	}
	printf("\n мінімальний елемент =  %d\n",min);
	system("pause");

	return 0;
}
